import ccy
from flask import request
from FlaskWebProject1 import app

@app.template_filter('fullname')
def fullname_filter(product):
    return '{0} / {1}'.format(product['category'], product['name'])

@app.template_filter('format_currency')
def format_currency_filter(amount):
    currency_code = ccy.countryccy(request.accept_languages.best[-2:])
    return '{0} {1}'.format(currency_code, amount)