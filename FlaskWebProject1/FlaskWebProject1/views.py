"""
Routes and views for the flask application.
"""

from datetime import datetime
from flask import render_template, request, jsonify
from FlaskWebProject1 import app
from FlaskWebProject1.models import PRODUCTS, Course, Student
from FlaskWebProject1.filters import format_currency_filter, fullname_filter

@app.route('/')
@app.route('/home')
def home():
    """Renders the home page."""
    return render_template(
        'home.html',
        title='Home',
        products=PRODUCTS
    )

@app.route('/product/<key>')
def product(key):
    product = PRODUCTS.get(key)
    if not product:
        print '404'

    return render_template('product.html', product=product)

@app.route('/test')
def test():
    a = Student('ben', 1)
    b = Student('go', 8)
    c = Course('science')
    c.students = [a, b]
    dict = { 'data': c.serialize }
    return jsonify(dict)

@app.context_processor
def processor():
    def fullname(product):
        return '{0} / {1}'.format(product['category'], product['name'])
    return { 'fullname': fullname }
