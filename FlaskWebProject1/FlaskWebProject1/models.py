PRODUCTS = {
    'iphone': {
        'name': 'iPhone 5S',
        'category': 'Phones',
        'price': 699,
    },
    'galaxy': {
        'name': 'Samsung Galaxy 5',
        'category': 'Phones',
        'price': 649,
    },
    'ipad-air': {
        'name': 'iPad Air',
        'category': 'Tablets',
        'price': 649,
    },
    'ipad-mini': {
        'name': 'iPad Mini',
        'category': 'Tablets',
        'price': 549
    }
}

class Student():
    def __init__(self, name, id):
        self.name = name
        self.id = id

    @property
    def serialize(self):
        return {
            'name': self.name,
            'id': self.id
        }

class Course():
    def __init__(self, name):
        self.name = name
        self.students = []

    @property
    def serialize_students(self):
        v = []
        for k in self.students:
            v.append(k.serialize)

        return v

    @property
    def serialize(self):
        return {
            'name': self.name,
            'students': self.serialize_students
        }

