"""
The flask application package.
"""

from flask import Flask
from FlaskWebProject1.utils import Momentjs

app = Flask(__name__)
app.jinja_env.globals['momentjs'] = Momentjs

from FlaskWebProject1 import views
